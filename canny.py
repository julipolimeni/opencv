import argparse
import cv2

'''
The Canny edge detector is a multi-step process. It involves
blurring the image to remove noise, computing Sobel gradient
images in the x and y direction, suppressing edges, and
finally a hysteresis thresholding stage that determines if a
pixel is “edge-like” or not.
'''
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
image = cv2.GaussianBlur(image, (5, 5), 0)
cv2.imshow("Blurred", image)
cv2.waitKey()

canny = cv2.Canny(image, 30, 150)
cv2.imshow("Canny", canny)
cv2.waitKey()
