from __future__ import print_function
import imutils
import argparse
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey()

'''The aspect ratio is the proportional relationship of the width and the
 height of the image.'''
r = 150.0 / image.shape[1]
dim = (150, int(image.shape[0]*r))
''' Another metods cv2.INTER_LINEAR, cv2.INTER_CUBIC, and cv2.INTER_NEAREST.'''
resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)
cv2.imshow("Resized", resized)
cv2.waitKey()

# Using imutils
resized = imutils.resize(image, width=500)
cv2.imshow("Resized by imutils", resized)
cv2.waitKey()

# Using imutils
resized = imutils.resize(image, height=500)
cv2.imshow("Resized by imutils", resized)
cv2.waitKey()
