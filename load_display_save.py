from __future__ import print_function
# Use argparse to handle parsing our command line arguments.
import argparse
# Import opencv library
import cv2

ap = argparse.ArgumentParser()
# The argument we need is "--image"
ap.add_argument("-i", "--image", required=True, help="Path to the image")
# Parse the argument and save them in a dictionary
args = vars(ap.parse_args())

# Import the image
image = cv2.imread(args["image"])
# Print some information about the image
print("width: {} pixels" .format(image.shape[1]))
print("height: {} pixels" .format(image.shape[0]))
print("channels: {}" .format(image.shape[2]))

# Display the actual image
cv2.imshow("Image", image)
# Pauses the execution of the script until we press a key on our keyboard
cv2.waitKey()

# Save the image
cv2.imwrite("newimage.png", image)
