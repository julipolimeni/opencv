from __future__ import print_function
import argparse
import imutils
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required="True", help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey()

# Center of the image
(h, w) = image.shape[:2]
center = (h//2, w//2)

# Define a matrix torotate the image
# cv2.getRotationMatrix2D(Center, Degrees, Scale)
M = cv2.getRotationMatrix2D(center, 45, 1.0)
rotated = cv2.warpAffine(image, M, (w, h))
cv2.imshow("Rotated by 45 Degrees", rotated)
cv2.waitKey()

# Using imutils
rotated = imutils.rotate(image, 75, center, 1.0)
cv2.imshow("Rotated with imutils", rotated)
cv2.waitKey()
