# Image transformation: translation
from __future__ import print_function
import argparse
import numpy as np
import cv2
import imutils

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=False, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)

'''
We first define our translation matrix M.
The first row of the matrix is [ 1, 0, t x ], where tx is the number of pixels we will
shift the image left or right.
Then, we define the second row of the matrix as [ 0, 1, t y ] ,where ty is the number
of pixels we will shift the image up or down.
'''
M = np.float32([[1, 0, 25], [0, 1, 50]])
shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Shifted Down and Right", shifted)
cv2.waitKey()

M = np.float32([[1, 0, -25], [0, 1, -50]])
shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
cv2.imshow("Shifted Up and Left", shifted)
cv2.waitKey()

# Test the imutils function
shifted = imutils.translate(image, 100, -120)
cv2.imshow("Shifted", shifted)
cv2.waitKey()
