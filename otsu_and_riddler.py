from __future__ import print_function
import argparse
import mahotas as mh
import cv2

'''
Another way we can automatically compute the threshold value of T is to
use Otsu’s method. Otsu’s method assumes there are two peaks in the
grayscale histogram of the image. It then tries to find an optimal
value to separate these two peaks – thus our value of T.
'''

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
blurred = cv2.GaussianBlur(image, (5, 5), 0)
cv2.imshow("Original", image)


# Otsu
T = mh.thresholding.otsu(blurred)
print("Otsu's threshold: {}".format(T))
thresh = image.copy()
thresh[thresh > T] = 255
thresh[thresh < T] = 0
thresh = cv2.bitwise_not(thresh)
cv2.imshow("Otsu's", thresh)
cv2.waitKey()

'''
Another way we can automatically compute the threshold value of T is to
use Riddler-Calvard method
'''
# Riddler-Calvard
T = mh.thresholding.rc(blurred)
print("Riddler-Calvard: {}".format(T))
thresh = image.copy()
thresh[thresh > T] = 255
thresh[thresh < T] = 0
thresh = cv2.bitwise_not(thresh)
cv2.imshow("Riddler-Calvard", thresh)
cv2.waitKey()
