import argparse
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)
cv2.waitKey()
# image[start y: ending y, start x: ending x]
cropped = image[30:120, 240:335]
cv2.imshow("Crop", cropped)
cv2.waitKey()
