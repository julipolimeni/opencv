import argparse
import cv2

'''
we can use adaptive thresholding, which considers small neighbors of pixels
and then finds an optimal threshold value T for each neighbor. This method
allows us to handle cases where there may be dramatic ranges of pixel
intensities and the optimal value of T may change for different parts
of the image
'''

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
blurred = cv2.GaussianBlur(image, (5, 5), 0)
cv2.imshow("Original", image)
cv2.waitKey()

thresh = cv2.adaptiveThreshold(
    blurred, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 4)
cv2.imshow("Mean Thresh", thresh)
cv2.waitKey()

thresh = cv2.adaptiveThreshold(
    blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 15, 3)
cv2.imshow("Gaussian Thresh", thresh)
cv2.waitKey()
