from __future__ import print_function
import numpy as np
import cv2

# Define the image
canvas = np.zeros((300, 300, 3), dtype="uint8")

for i in range(0, 15):
    radius = np.random.randint(5, high=200)
    color = np.random.randint(0, high=256, size=(3,)).tolist()
    pt = np.random.randint(0, high=300, size=(2,))
    cv2.circle(canvas, tuple(pt), radius, color, -1)

# Show the image
cv2.imshow("Circles", canvas)
cv2.waitKey(0)

# Define another image
canvas2 = np.zeros((300, 300, 3), dtype="uint8")

for i in range(0, 15):
    pt1 = np.random.randint(0, high=300, size=(2,))
    pt2 = np.random.randint(0, high=300, size=(2,))
    color = np.random.randint(0, high=256, size=(3,)).tolist()
    cv2.rectangle(canvas2, tuple(pt1), tuple(pt2), color, -1)

# Show the Image
cv2.imshow("Rectangles", canvas2)
cv2.waitKey(0)
