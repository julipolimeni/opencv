from __future__ import print_function
# Use argparse to handle parsing our command line arguments.
import argparse
# Import opencv library
import cv2

ap = argparse.ArgumentParser()
# The argument we need is "--image"
ap.add_argument("-i", "--image", required=True, help="Path to the image")
# Parse the argument and save them in a dictionary
args = vars(ap.parse_args())

# Import the image
image = cv2.imread(args["image"])
# Displat the image
cv2.imshow("Original", image)

# Getting and printing iformation of a pixel
(b, g, r) = image[0, 0]
print("Pixel at [0, 0] - Red: {}, Green: {}, Blue: {}" .format(r, g, b))

# Setting the value of a pixel
image[0, 0] = (0, 0, 255)
(b, g, r) = image[0, 0]
print("Pixel at [0, 0] - Red: {}, Green: {}, Blue: {}" .format(r, g, b))

# Getting the left corner of the image
corner = image[0:100, 0:100]
cv2.imshow("Corner", corner)

# Setting the left corner of the image
image[0:100, 0:100] = (0, 0, 255)
cv2.imshow("Updated", image)

# Pauses the execution of the script until we press a key on our keyboard
cv2.waitKey()
