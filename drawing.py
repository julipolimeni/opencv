from __future__ import print_function
import numpy as np
import cv2

# Define the canvas
canvas = np.zeros((300, 300, 3), dtype="uint8")

# Draw a line
green = (0, 255, 0)
red = (0, 0, 255)
# cv2.line(img, first point, second point, color, thickness)
cv2.line(canvas, (0, 0), (300, 300), green, 4)
cv2.imshow("Line", canvas)
cv2.waitKey(0)

# Draw a rectangle
# cv2.rectangle(img, first point, second point, color,thickness(if<0 fill the rectangle))
cv2.rectangle(canvas, (15, 15), (250, 250), red, 3)
cv2.imshow("Rectangle and line", canvas)
cv2.waitKey(0)

# Draw a circle
canvas2 = np.zeros((300, 300, 3), dtype="uint8")
(centerX, centerY) = (canvas2.shape[1]//2, canvas2.shape[0]//2)
white = (255, 255, 255)

for r in range(0, 175, 25):
    # cv2.circle(img, center, radius, colour, thickness(if<0 fill the circle))
    cv2.circle(canvas2, (centerX, centerY), r, white)

cv2.imshow("Circles", canvas2)
cv2.waitKey(0)
