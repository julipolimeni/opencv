import numpy as np
import argparse
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)

# Averaging method
'''
we are going to define a k × k sliding window on top of our image,
where k is always an odd number. This window is going to slide
from left-to-rightand from top-to-bottom. The pixel at the center
of this matrix is then set to be the average of all other pixels
surrounding it.
We call this sliding window a “convolution kernel” or just a
“kernel”
cv2.blur(image, kernel)
'''

blurred = np.hstack([
    cv2.blur(image, (3, 3)),
    cv2.blur(image, (5, 5)),
    cv2.blur(image, (7, 7))])

cv2.imshow("Averaged", blurred)
cv2.waitKey()

# Gaussian method
'''
Gaussian blurring is similar to average blurring, but instead of
using a simple mean, we are now using a weighted mean,where
neighborhood pixels that are closer to the central pixel
contribute more “weight” to the average.
cv2.GaussianBlur(image, kernel, standard deviation)
'''

blurred = np.hstack([
    cv2.GaussianBlur(image, (3, 3), 0),
    cv2.GaussianBlur(image, (5, 5), 0),
    cv2.GaussianBlur(image, (7, 7), 0)])

cv2.imshow("Gaussian", blurred)
cv2.waitKey()

# Median method
'''
As in the averaging blurring method, we consider all pixels
in the neighborhood of size k × k. But, unlike the averaging
method, instead of replacing the central pixel with the average
of the neighborhood, we instead replace the central pixel with
 the median of the neighborhood.
'''

blurred = np.hstack([
    cv2.medianBlur(image, 3),
    cv2.medianBlur(image, 5),
    cv2.medianBlur(image, 7)])

cv2.imshow("Median", blurred)
cv2.waitKey()

# Bilateral method
'''
Bilateral blurring accomplishesthis by introducing two Gaussian
distributions.
The first Gaussian function only considers spatial neighbors,
that is, pixels that appear close together in the (x, y)coordinate
space of the image. The second Gaussian then models the pixel
intensity of the neighborhood, ensuring that only pixels with
similar intensity are included in the actual computation of the blur.
Overall, this method is able to preserve edges of an image, while still
reducing noise
'''

blurred = np.hstack([
    cv2.bilateralFilter(image, 5, 21, 21),
    cv2.bilateralFilter(image, 7, 31, 31),
    cv2.bilateralFilter(image, 9, 41, 41)])

cv2.imshow("Bilateral", blurred)
cv2.waitKey()
